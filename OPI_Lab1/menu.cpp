#include <iostream>
#include "Tasks.h"
#include "menu.h"

using namespace std;

void menu(int* matrix[], int len, int lowLine, int upLine)
{
    char var{};
    bool exit = true;
    while (exit == true) {

        PrintMatrix(matrix, len);

        cout << "�������� �������� ����� ���� ��� ������ � ��������:" << endl << endl;
        cout << "   a) ����� ������������ ������� ������ ������ � ������, ������� �������� ���������� ������������ �������." << endl;
        cout << "   b) ����� ����� ������� ��������� � ������ ������, ������������� ����� ������� �������������� �������� � ������." << endl;
        cout << "   c) ����� ������ �������������  ������� ����� ��������� �������, ���������� ������ ������ (��. ���.)." << endl;
        cout << "   d) ������������� ��������� ������� �� ��������." << endl;
        cout << "   e) ����������� ����� ������� �������." << endl;
        cout << "   f) �������� ������� � ������������������ ����." << endl;
        cout << "   g) ����� ����� ���� ������������� ��������� �������." << endl;
        cout << "   h) ����� �� ����." << endl;

        cout << endl << "��� �����: ";
        cin >> var;

        switch (var) {
        case 'a':
            MaxElements(matrix, len, lowLine, upLine);
            break;
        case 'b':
            SumElements(matrix, len);
            break;
        case 'c':
            FirstDenied(matrix, len);
            break;
        case 'd':
            PrintMatrix(SortDecr(matrix, len), len);
            break;
        case 'e':
            PrintMatrix(MoveElem(matrix, len), len);
            break;
        case 'f':
            TriangularMatrix(matrix, len);
            break;
        case 'g':
            SumDenied(matrix, len);
            break;
        case 'h':
            break;
        default:
            cout << endl << "! �������� ���� !" << endl;
        }

        cout << endl << "���� ������ ���������� ������, ������� 1. ���� ������ ����� �� ����, ������� 0: ";
        cin >> exit;
        system("cls");
    }
}