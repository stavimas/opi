#ifndef MENU_H
#define MENU_H

void menu(int* [], int, int, int);
int** CreateMatrix(int, int, int);
void DeleteMatrix(int* [], int);

#endif // MENU_H