#include <iostream>
#include <iomanip>
#include <ctime>
#include "Tasks.h"

#define RANDOM rand()%(up_line - low_line + 1) + low_line

using namespace std;

// �������� ������������ ���������� �������
int** CreateMatrix(int len, int low_line, int up_line)
{
    srand(time(0));
    int** matrix = new int* [len] {};

    for (int i{}; i < len; i++) {
        matrix[i] = new int[len] {};
    }

    for (int i{}; i < len; i++) {
        for (int j{}; j < len; j++) {
            matrix[i][j] = RANDOM;;
        }
    }

    return matrix;
}

// ����� ������� �� �����
void PrintMatrix(int* matrix[], int len)
{
    cout << "���� �������:" << endl;

    for (int i{}; i < len; i++) {
        for (int j{}; j < len; j++) {
            cout << setw(5) << matrix[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

// ����� ������������� �������� � ������ ������ ������� � �������(������) ������������ �������� �� ����� ������
void MaxElements(int* matrix[], int len, int low_line, int up_line)
{
    int* matr = new int[len] {};

    cout << endl << "������������ �������� �����:" << endl;

    for (int i{}; i < len; i++) {
        int max = low_line - 1;
        for (int j{}; j < len; j++) {
            if (matrix[i][j] > max) {
                max = matrix[i][j];
            }
        }
        matr[i] = max;
        cout << matr[i] << " ";
    }

    int min = up_line + 1;
    int pos{};

    for (int i{}; i < len; i++) {
        if (matr[i] < min) {
            min = matr[i];
            pos = i;
        }
    }

    cout << endl << endl << "���������� ������������ ������� ��������� � " << pos + 1 << " ������" << endl;

    delete[] matr;
}

// � ������ ������ ������ ������ ������������� ����� � ������ � ���������� �������� ������� ���� ����������� ����� � ������
void SumElements(int* matrix[], int len)
{
    int num = -1;
    int sum{};
    cout << endl;
    for (int i{}; i < len; i++) {
        for (int j{}; j < len; j++) {
            if (matrix[i][j] < 0) {
                num = j;
                break;
            }
        }
        if ((num == len - 1) || (num == -1)) {
            sum = 0;
        }
        else {
            for (int k = num + 1; k < len; k++) {
                sum += abs(matrix[i][k]);
            }
        }
        cout << "����� ������� ��� " << i + 1 << " ������:" << sum << endl;
        num = -1;
        sum = 0;
    }
}

// ����� ������� �������������� �������� � ���������� ������� �������
void FirstDenied(int* matrix[], int len)
{
    int denElement{};
    for (int i{}; i < len && denElement >= 0; i++) {
        for (int j = round(len / 3); j < len; j++) {
            
                if (matrix[i][j] < 0) {
                    denElement = matrix[i][j];
                    break;
                }
        }
    }
    if (denElement < 0) {
        cout << endl << "������ ������������� ������� � ���������� ������� �������: " << denElement << endl;
    }
    else {
        cout << "�������������� �������� ���." << endl;
    }
}

// �������� ������������ ���������� �������
void DeleteMatrix(int* matrix[], int len)
{
    for (int i{}; i < len; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
    cout << "������� ������� �������." << endl;
}

// ���������� ���������� ������� �� ��������
int** SortDecr(int* matrix[], int len)
{
    cout << "\n����� ������� ������ ������������� �� ��������?\n>> ";
    int col{};
    cin >> col;

    int** copyMatrix = new int* [len] {};
    copyMatrix = matrix;

    int elem;
    for (int i = 0; i < len; i++) {
        for (int k = 0; k < len; k++) {
            if (copyMatrix[i][col - 1] > copyMatrix[k][col - 1])
            {
                elem = copyMatrix[i][col - 1];
                copyMatrix[i][col - 1] = copyMatrix[k][col - 1];
                copyMatrix[k][col - 1] = elem;
            }
        }
    }

    return copyMatrix;
}

// ����������� ������ ������� �������
int** MoveElem(int* matrix[], int len)
{
    int** addMatrix = new int* [len] {};
    for (int i = 0; i < len; i++) {
        addMatrix[i] = new int[len];

        for (int j = 0; j < len; j++) {
            addMatrix[i][j] = matrix[i][j];
        }
    }

    for (int i = len / 3; i < len * 2 / 3; i++) {
        for (int j = len / 3; j < len * 2 / 3; j++) {
            addMatrix[i][j] = matrix[i - len / 3][j + len / 3];
        }
    }

    for (int i = 0; i < len / 3; i++) {
        for (int j = len * 2 / 3; j < len; j++) {
            addMatrix[i][j] = matrix[i + len / 3][j - len / 3];
        }
    }

    cout << "����� ����� ������� ���� ���������� �� ������: ����������� ���� ��������� ������ � ������� ������\n\n";

    return addMatrix;
}

 //���������� ������� � ������������ ����
void TriangularMatrix(int* matrix[], int len)
{
    for (int i = 0; i < len - 1; i++){

        for (int j = i + 1; j < len; j++){

            double koef = double(matrix[j][i])/ double(matrix[i][i]);

            for (int k = i; k < len; k++){

                matrix[j][k] = double(matrix[j][k]) - double(matrix[i][k]) * koef;
            }
        }
    }
    PrintMatrix(matrix, len);
}

//����� ������������� ��������� �������
void SumDenied(int* matrix[], int len)
{
    int sum{};
    for (int i = 0; i < len; i++) {
        for (int j = 0; j < len; j++) {
            if (matrix[i][j] < 0)
            {
                sum += matrix[i][j];
            }
        }
    }

    cout << "����� ������������� ��������� ������� ����� -> " << sum << endl;
}
